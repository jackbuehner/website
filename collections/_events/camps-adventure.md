---
layout: md
title: Camps/High Adventure
subtitle: This page provides information about summer/winter camps, high adventure camps, and other special trips.  Registration, dates and other information can be found in either the event descriptions below or via the <a href="/calendar.html">troop calendar</a>. Information about the Troop's regular overnight trips can be found on the <a href="/calendar.html">troop calendar</a> or on the <a href="/campout-schedule-local-events.html">campout schedule page</a>.
menu: hide
div_wrapper_extra: ", .md-page-content h3"
redirect_from: camps-adventure.html
---

## 2020 Camps & High Adventure 
{:.mdc-typography--headline4}

---

### Summer Camp at Woodruff Scout Reservation
{:.mdc-typography--headline5}

June 7 - 13, 2020

When the Atlanta Area Council talks about “the summer of a lifetime,” they’re not just talking about a fun week with your best friends. This is where thousands of Scouts come together for life-changing, memory-making, adventure-taking awesomeness that will impact your life forever. Troop 370 regularly brings 65-75 Scouts to Woodruff.

[Sign up for Woodruff](/members/woodruff-sc-sign-up.html){:.mdc-button.md}

### Philmont Scout Ranch at Cimarron, NM
{:.mdc-typography--headline5}

June 27 - July 10, 2020

Philmont offers 140,177 acres of rugged mountain wilderness in the Rocky Mountains in northeastern New Mexico. In addition to providing an unforgettable adventure in backpacking across miles of rugged, rocky trails, Philmont Scout Ranch offers programs that feature the best of the Old West—horseback riding, burro packing, gold panning, chuckwagon dinners, and interpretive history—with exciting challenges for today such as rock climbing, mountain biking, and sport shooting. It’s an unbeatable recipe for fast-paced fun in the outdoors.

[Sign up for Philmont](/members/high-adventure-2020-sign-up.html){:.mdc-button.md}

### New River Trek at Summit Bechtel Reserve, WV
{:.mdc-typography--headline5}

July 4 - 11, 2020

This is a 50-mile paddle trek on the New River. You will navigate the upper portion of the river in inflatable kayaks (one or two-man) called “duckies”. This portion of the river includes Class I to III rapids. The final whitewater day in the Lower Canyon is a rafting experience and includes Class III to V rapids. Crews camp along the river through the New River Gorge. Summit Bechtel Reserve requires all participants to be 14 years old by the first day of the trek.

[Sign up for New River Trek](/members/high-adventure-2020-sign-up.html){:.mdc-button.md}

### Northern Tier National High Adventure Base at Atikokan, Ontario
{:.mdc-typography--headline5}

July 23 - August 2, 2020

Explore millions of acres of pristine lakes, meandering rivers, dense forests and wetlands in Northwest Ontario on a nine day canoeing adventure in Quetico Provential Park. North America’s Canoe Country, a vast series of navigable lakes and rivers spanning thousands of square miles, is one of the last great wildernesses on the continent.

[Sign up for Northern Tier](/members/high-adventure-2020-sign-up.html){:.mdc-button.md}

### Winter Camp at Bert Adams Scout Reservation
{:.mdc-typography--headline5}

December 27 - 31, 2020

Winter Camp, one of the most unique resident camp experiences available to Scouts in Atlanta, includes over 30 merit badges and, including unique programs and merit badges that are not available at either of our summer camps. This camp includes 3 full days of activities, 4 nights camping in platform tents, 11 meals, and 3 evening cracker barrels.

---

## 2021 Camps & High Adventure
{:.mdc-typography--headline4}

---

### Okpik Cold Weather Camping Adventure at Atikokan, Ontario
{:.mdc-typography--headline5}

February 11 - 15, 2021

Okpik Cold Weather Camping, Northern Tier’s winter offering, is the BSA’s premier winter camping program. At Okpik, Scouts experience a true Northwoods winter: learning how to thrive in subzero temperatures, travel across frozen wilderness lakes and construct their own sleeping structures out of snow. All trips are fully outfitted and provisioned, including almost all of the personal gear necessary to stay warm in the winter. A highly trained staff member, called an Interpreter, accompanies all crews on their trek.

[Sign up for Okpik](/members/okpik-2021-sign-up.html){:.mdc-button.md}

### Sea Base St. Thomas Sailing Adventure at St. Thomas, U.S. Virgin Islands
{:.mdc-typography--headline5}

June 28 - July 4, 2021

Experience the allure of the U.S. Virgin Islands. Here, the trade winds blow, offering some of the best sailing in the world. Upon arrival, your crew will board a 40-foot plus vessel with an experienced captain. Most units attain the 50 Miler Award as they circumnavigate the crystal blue waters surrounding the island of St. John. In addition to sailing, your unit will snorkel pristine coral reefs, hike in Virgin Islands National Park, swim ashore to incredible beaches, and more!

[Sign up for Sea Base St. Thomas](/members/sea-base-2021-sign-up.html){:.mdc-button.md}

### Sea Base Florida Scuba Live Aboard at Islamorada, FL
{:.mdc-typography--headline5}

June 26 – July 3, 2021

Sea Base has combined scuba with a live aboard experience to create one incredible adventure. You will spend your days discovering the Florida Keys National Marine Sanctuary above and below the water’s surface. With a crew size of 10 to 12 people, your home will be aboard one or two vessels depending on availability and schedule. During this adventure, you will dive 11 or more times including night dives (weather permitting). When you are not diving, there will be opportunities for fishing and cruising the Florida Keys. You must be a certified diver to participate in this program. Medical restrictions apply. Call Sea Base for more details.

[Sign up for Sea Base Scuba Live Aboard](/members/sea-base-2021-sign-up.html){:.mdc-button.md}

### 2021 BSA National Jamboree at Summit Bechtel Reserve, WV
{:.mdc-typography--headline5}

July 21 - 30, 2020

Scouting’s flagship event is one-of-a-kind. It’s a gathering of tens of thousands of Scouts, leaders, and Jamboree Service Team members that showcases everything that is great about the Boy Scouts of America. Over the course of 10 summer days, once every four years, the Boy Scouts of America gathers together. Scouts and Scouters who attend will explore all kinds of adventures—stadium shows, pioneer village, Mount Jack hikes, adventure sports and more—in the heart of one of nature’s greatest playgrounds. With 10,000 acres at the Summit to explore, there’s no shortage of opportunities to build Scouting memories.

[Sign up for National Jamboree 2021](https://jamboree.scouting.org/){:.mdc-button.md}
