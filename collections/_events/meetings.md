---
layout: md
title: Meetings
long_title: Troop 370 Meetings
subtitle: Troop 370 has regular Wendesday night meetings and monthly planning meetings. <a href="/participation.html">Learn how you can participate.</a>
columns: 2
menu: hide
---
<style>
  .md-page-content {
    text-align: center;
  }
</style>

## Weekly Meetings

Wednesday Nights <br>
7:30 PM - 9:00 PM  <br>
4400 Peachtree Dunwoody Rd, Atlanta, GA 30342 <br>
Activities Building <br>
(Loridans Drive Entrance)

## Planning Meetings

Patrol Leaders' Council <br>
First Wednesday night of each month at 6:30 PM

Troop Committee <br>
Second Monday of each month at 6:30 PM