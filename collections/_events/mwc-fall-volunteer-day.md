---
layout: md
title: Mountain Way Common Fall Volunteer Day
subtitle: Saturday, September 8, 2019<br>8:30 AM - 12:30 PM
headings: false
menu: hide
redirect_from: mwc-fall-volunteer-day.html
---

Mountain Way Common will be having a special Fall Volunteer Day on Saturday, September 28 from 8:30 am to 12:30 pm. Among other projects, we will be helping **William Sharp construct his Eagle Project** and **Matthew Keagle repair/maintain his Eagle Project**. These volunteer hour will qualify as **conservation service hours**. **Food Trucks** will be at the park starting at noon so please **bring $10.00 if you are interested in eating**. Specifics are as follows:

- **Date/Time:** Saturday, September 28th from 8:30 am – 12:30 pm
- **Location:** Mountain Way Common - approximately 4124 North Ivy Rd NE, Atlanta, GA 30342 (please note this address is for the house next to the park) Parking: Street parking is available along N. Ivy Rd. Please be respectful of neighbors and not block driveways or mailboxes. We will meet at the N. Ivy Rd. entrance into the park.
- **Bathroom:** There is NO bathroom on-site at this park. Please come prepared.
- **Project:** Volunteers will use hand tools to improve trails in the park, clean up a new creek overlook, remove invasive vines from the trees and clear invasive plants from the trailhead and throughout the park.
- **What to Wear:**
  - Please wear closed toe shoes (boots or sneakers) -- sandals are NOT appropriate
  - Please wear clothes and shoes that can get dirty
  - Long pants and long sleeves are recommended -- this limits small bumps and scratches
- **What to Bring:**
  - Please bring water for yourself in a reusable waterbottle
  - If you have a comfortable pair of work gloves, feel free to bring them along
- **What is Provided:** All tools and gloves will be provided by FoMWC

**This is a rain or shine workday** (one advantage of being under an overpass is that we have lots of dry areas)

[<i class="material-icons mdc-button__icon">navigation</i> Directions](https://www.bing.com/maps?q=Mountain+Way+Common+Atlanta%2C+GA+30342&PC=U531&cvid=20e6a562e1df43418f8426d3fdba73b1&setLang=en&DAF0=1){:.mdc-button.mdc-button--outlined.md}