---
layout: md
title: Woodruff Merit Badge Selections 2020
long_title: Merit Badge Selections for Woodruff Summer Camp (2020)
headings: false
menu: hide
---

It's time to make your merit badge selections for summer camp! Fill out this form before March 23rd so we know what to register for.

If you have any questions contact Jeff Hunt.

[Sign Up for Merit Badges](https://forms.gle/MzXumknnn31BDW9m6){:.mdc-button.mdc-button--outlined.md}