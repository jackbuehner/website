---
layout: md
title: Pine Straw
long_title: Troop 370 Pine Straw
menu: hide
---

## About

The Troop’s principal fund raiser for summer camp and high adventure is selling and delivering pine straw to neighbors in the Buckhead, Brookhaven, and Sandy Springs areas.

We sell premium full-sized bales of pine straw.

Pine straw can be delivered to your house by scouts or picked up at the church. [Click here for directions.](https://www.google.com/maps/dir/''/St.+James+United+Methodist+Church+4400+Peachtree+Dunwoody+Road+NE+,+Atlanta+30342)  
Scouts are available to go to the houses and spread.

### 2019-2020 Dates

Fall Pine Straw Sales Date – November 2, 2019  
Spring Pine Straw Sales Date – April 25, 2020 (post COVID-19 adjustment)*

*Note: If you ordered pine straw for delivery on March 28, you will receive an email allowing you to cancel your order if the April delivery is too late for you

## Pricing

Delivery to your house: $10.00 per order<span class="indent--new-line">FREE DELIVERY for orders of 30 or more bales</span>Spread pine straw: $5.00 per bale

Funds will be used to send Scouts from Troop 370 to Summer Camps and High Adventure Camps.

## Contact

If you have questions, please email: [370pinestraw@gmail.com](mailto:370pinestraw@gmail.com)

## Order

Order pine straw through the online store (preferred) or by using the order form below:

[<svg class="mdc-button__icon" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height:24px;"><path d="M17,18C15.89,18 15,18.89 15,20A2,2 0 0,0 17,22A2,2 0 0,0 19,20C19,18.89 18.1,18 17,18M1,2V4h2L6.6,11.59L5.24,14.04C5.09,14.32 5,14.65 5,15A2,2 0 0,0 7,17H19V15H7.42A0.25,0.25 0 0,1 7.17,14.75C7.17,14.7 7.18,14.66 7.2,14.63L8.1,13H15.55C16.3,13 16.96,12.58 17.3,11.97L20.88,5.5C20.95,5.34 21,5.17 21,5A1,1 0 0,0 20,4H5.21L4.27,2M7,18C5.89,18 5,18.89 5,20A2,2 0 0,0 7,22A2,2 0 0,0 9,20C9,18.89 8.1,18 7,18Z" /></svg> Online store](https://troop370atlanta.org/pay/pinestraw.html){:.mdc-button.mdc-button--outlined.md} [<svg class="mdc-button__icon" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height:24px;"><path d="M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z"></path></svg> Order Form - Spring 2020](/src/documents/Troop370PineStrawFlyerSpring2020.pdf){:.mdc-button.mdc-button--outlined.md}
