---
layout: md
title: Camp Cards
long_title: Camp Cards Fundraiser
menu: hide
---

## Looking to purchase a Camp Card?

Camp cards are $5 per card. Scouts will be selling camp cards from January 22, 2020 until the end of April.

More information about Camp Cards, including the offers on this year's cards, can be viewed on the Atlanta Area Council website.

[Camp Card info<span class="hide-small">rmation</span>](http://onelink.to/zryeqw){:.mdc-button.mdc-button--outlined.md}

---

## Selling Information for Scouts (Announcement)

Camp Cards will be available for purchase starting at the January 22d meeting and onward through mid-April. The cards are $2.50 each and you may pay in cash, check, or via the Troop Venmo account. Camp Card rep. Katie Kaplan will be at Wednesday meeting to explain the Camp Card fundraising program, including this year’s rewards for sales.

[Meeting schedule](/events/meetings.html){:.mdc-button.mdc-button--outlined.md}