---
layout: md
title: Eagle Scout Honor Roll
subtitle: Troop 370 has a proud tradition of Eagle Scouts dating back to our first Eagle, George Takis, in 1965. Through the PDF file below you can view the impact that our Eagle Scouts, now over 200 in number, have had on their community through their Eagle Service Projects. What is more, our Troop 370 Eagle Scouts have continued to make their mark on the world through their service and leadership as adults.</p><p><a href="#eagle-list" class="mdc-button mdc-button--outlined">View Eagle honor roll list</a>
headings: false
nav_order: 3
redirect_from: eagle-scout-honor-roll.html
---

<iframe src="https://www.batchgeo.com/map/a30ec08cf79f88a9f9a28322cda3d34c" class="project-map" style="width:100%;border:1px solid gray;height: 600px;"></iframe>

<div id="eagle-list">
  <div data-type="AwesomeTableView" data-viewID="-LeHRg8tkfy3ys8zNjXK"></div>
</div>
  
