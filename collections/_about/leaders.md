---
layout: md
title: Leaders
long_title: Current Leadership
nav_order: 2
redirect-from: leaders.html
---

## Scouts

Senior Patrol Leader: 

Assistant Senior Patrol Leader over Patrols: 

Assistant Senior Patrol Leader over Quartermasters, Scribes, Librarians, Historians, and Webmasters: 

Assistant Senior Patrol Leader over Instructors, Troop Guides, Den Chiefs, and Gymmasters: 

## Scouters

Scoutmaster: Harry Evans<br>
<a href="contact.html">Contact</a>

Troop Committee Chairman: John Buehner<br>
<a href="contact.html">Contact</a>

Chartered Organization Representative: Kent Watkins<br>
<a href="contact.html">Contact</a>
