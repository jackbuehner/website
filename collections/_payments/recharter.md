---
layout: md
title: Annual Recharter
menu: hide
---
<style>
  #its-time-to-recharter {
    display: none;
  }
</style>

## It's time to recharter

It's time to renew your registration (recharter). Each year the Troop and St James UMC renews their affiliation with the Boy Scouts of America. Recharter is also when individual youth and adult members of Troop 370 renew their memberships with the Troop.

As in past years, a detailed recharter notice will be sent to each Troop 370 household. It lists those currently registered and the total fee due from that household for 2018\. Check your email for more details.

Please make every effort to complete the recharter process by November 30th.

Choose one of the payment options below to recharter.

## Venmo (Preferred)

This year, the Troop is offering the option of making your payment using Venmo. Troop 370 prefers Venmo because merchant fees are waived for payments that don't use a credit card. You must make payments via the Venmo app. When paying, enter the Scout’s name and “Recharter” (e.g. John Doe / Recharter) with your payment in the 'What's it for?' section.  
You may use only your last name if your family has more than one member renewing their registration.

To pay, download the app and add Troop-ThreeSeventy as a Venmo friend. Alternatively, you can click the button below to go to the Troop 370 Venmo page.

[<svg class="mdc-button__icon" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height:24px;"><path d="M19.5,3C20.14,4.08 20.44,5.19 20.44,6.6C20.44,11.08 16.61,16.91 13.5,21H6.41L3.56,4L9.77,3.39L11.28,15.5C12.69,13.21 14.42,9.61 14.42,7.16C14.42,5.81 14.19,4.9 13.83,4.15L19.5,3Z"></path></svg> Pay on Venmo](https://venmo.com/troop-threeseventy){:.mdc-button.mdc-button--outlined.md} [Get app](http://onelink.to/zryeqw){:.mdc-button.mdc-button--outlined.md}

## PayPal

Like last year, the Troop is offering the option of making your payment using PayPal. Although PayPal will impose merchant fees for annual dues (which are not eligible for Friends and Family treatment), we understand that many scout families prefer PayPal over Venmo. You may pay using the button below.  
Simply follow the online instructions. In the box entitled "Scout Name/Event", enter "John Doe/Recharter." As with Venmo, you may use only your last name if your family has more than one member renewing their registration.

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank"><input type="hidden" name="cmd" value="_s-xclick"> <input type="hidden" name="hosted_button_id" value="SXVTG8VBCPUNJ"> <input id="paypal-button" type="image" src="https://raw.githubusercontent.com/troop-370/troop370/master/src/icons/pay-using-paypal-button.png" name="submit" alt="PayPal - The safer, easier way to pay online!" border="0" style="height: 36px;"></form>


## Check and Snail Mail:

If you prefer to pay by check instead of Venmo or PayPal:

- Please make your check payable to “Troop 370”, clearly noting on the check that it is for RECHARTER and including the full name(s) of the scout(s) and adult(s) covered by the payment.
- Mail the check – and your email address - no later than November 30 to Lori Ainsworth at 5221 Riverview RD NW, Atlanta, GA 30327.
