---
layout: md
title: Merit Badge Counselors
headings: false
redirect_from: mb-counselors.html
---

If you are a Scout interested in working on a Merit Badge, the files below show the Troop 370 adults who are registered Merit Badge Counselors and provide contact information so that you can contact them and set up a time to discuss the Merit Badge requirements.

If you are an adult, please consider being a Merit Badge Counselor. There are over 100 merit badges, and being a counselor allows you to share your vocation and hobbies with Scouts.

This document was last updated on 19 March 2020

[<svg class="mdc-button__icon" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height:24px;"><path d="M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z"/></svg> Merit Badge Instructor List](/src/documents/rosters/Troop 370 Merit Badge Counselors 29MARCH2020.pdf){:.mdc-button.mdc-button--outlined.md}