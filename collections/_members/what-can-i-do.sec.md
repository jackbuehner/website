---
layout: md
title: What Can I Do?
subtitle: Troop 370 is a large troop that serves well over one hundred active scouts, and there are plenty of ways for our parents and adult volunteers to assist throughout the year. While we encourage you to be as active and involved as you would like to be, you do not have to make a full-time, every week commitment. Many of our parent volunteers never go on a campout and never put on a uniform, but their contributions are critical to the efficient operation of Troop 370 and the training of its Scouts. Look through the list below, and see what calls out to you.
---
<style>
  #markdown-toc > li:first-of-type {
    display: none;
  }
</style>

## Table of Contents

* Table of contents
{:toc}

## Board of Review Team Members

<u>Board of Review Coordinator (Steve Shindell)</u> – Coordinate scheduling 3-5 adults every week to serve as Board of Review members for Scout advancement.

<u>Board of Review Members</u> – Agree to serve of Boards of Review.  With sufficient participation, Board Members would only need to serve once or twice a month.   The Troop has a comprehensive list of guidelines and sample questions. 

Steve Shindell is the current Board of Review Coordinator.  Contact Steve if you are interested in serving regularly as a Board of Review member,

## Troop Campouts

<u>Trip Captain</u> – Works with the Troop’s Campout Coordinator to create and collect Permission Slips, collect campout fees, create campout roster, confirm “accommodations” and complete and file the BSA Trip Permit.

<u>Trailer King or Queen</u> – volunteer to pull the Troop Trailer.   We have experts that will help you hook up the trailer to your truck or SUV and teach you how to back up with the trailer attached.

<u>Troop Closet Adult</u> – Assists Troop Quartermaster before campout departure, including assisting with Patrol and Scout Checkout procedures

<u>Health Forms</u> – Work with Campout Coordinator, Trip Captain and Health & Safety Coordinator to be sure that current Health Forms are on file (and at the campout) for all Scouts and parents. 

<u>Tent and Tarp Preservation</u> – If you have a an unfinished basement or large garage, volunteer to take home tents, ground cloths and tarps after a campout to be sure that they are completely dry before storing them in the Troop Closet. 

Cyrus Sharp is the current Campout Coordinator.  Contact Cyrus if you would like to assist with Troop campouts.

## Health and Safety<span style="text-decoration:none"></span>

<u>Health Form Tracker</u> (Sandy Pon) – Be responsible for collecting and tracking Health Forms for Troop 370 Scouts and Scouters.   Provide notices to Scouts and parents prior to the expiration of the BSA Health Forms.

<u>Youth Protection Tracker</u> – Be responsible for tracking adult Scouters’ compliance with the BSA’s Youth Protection requirements, including notification to any adults whose YPT Certificate is expiring soon. 

<u>Health Form Collection</u> – Be one of the designated individuals to receive Health Forms at weekly Scout meetings.

<u>Special Training Tracker</u> - Be responsible for tracking what adults have gone through special safety training, including Wilderness First Aid, CPR, Safety Afloat, etc.   Such completions and certifications are often required as part of a Troop Event.

Sandy Pon is the Troop’s current Health and Safety Coordinator.  Contact Sandy if you are interested in assisting with the Troop’s health and safety efforts.

## Troop Registration Duties

<u>Registration Paperwork</u> – Be one of the designated individuals to receive Registration Forms, Dues Payments and Health Forms from new Scouts and parents at weekly Scout meetings.

<u>Rechartering Paperwork</u> (Lori Ainsworth) – Assist the Registration Coordinator with rechartering and dues collections in November and December each year.

Walter Waddey is the current Registration Coordinator.   Contract Walter if you are interested in assisting with Registration and Rechartering efforts.

## Adult Training<span style="text-decoration:none"></span>

<u>Adult Training Record Keeper_._ </u>(Sheri Buehner) Be the designated individual to track the Adult Learning Courses taken by the adult Scouters.  Also work with the District Liaison to confirm that adult training is accurately reflected in the District records.

<u>Adult Advancement_._ </u>Be the designated individual to work with the Adult Training Record Keeper to determine when Adult Scouters may be eligible for adult recognition awards and encourage adults to complete the appropriate applications and paperwork to be recognized.

Trigger Williams is the Troop’s Advancement Coordinator and routinely interacts with the Phoenix District on Adult Training requirements.  Contact Trigger if you are interested in assisting with Adult Training.

## Calendar Coordinator<span style="text-decoration:none"></span>

Be the contact person for keeping the Troop’s Google Calendar up to date

## Merit Badge Weekends (twice a year – February and August)

<u>Registration</u> - Arrive and set up Registration Area before the Event and help register Scouts and collect money

<u>Counselor Recruitment</u> – Contact past merit badge counselors and recruit new counselor to teach merit badges

<u>Lunch</u> – Help prepare and serve lunch

<u>Grocery Getter</u> - Volunteer to make a Costco or Sam’s Club Run for provisions

<u>Merit Badge Counselor</u> – Serve as a merit badge counselor or assistant

<u>Gym Supervisor</u> - Volunteer to supervise gym activities between the end of the merit badge class and parents’ arrival for pickup.

Shelli Keagle is the coordinator for the Troop Merit Badge Clinics.  Contact Shelli if you are interested in assisting with the Merit Badge Clinics or are willing to lead the organization of these Clinics in the future.

## Monthly Merit Badge and Scout Skill Programs and Merit Badge Counseling

<u>Merit Badge Counselor</u> – Sign up to be a Merit Badge Counselor.  There are over 100 merit badges.  Being a counselor allows you to share your vocation and hobbies with Scouts.   Some counselors will teach classes at the bi-annual Merit Badge Clinics or the monthly Troop 370 classes.  Others prefer to teach smaller group of Scouts on an ad-hoc basis.

<u>Monthly Merit Badge Program Coordinator</u> – (Sheri Buehner) Work with registered merit badge counselors to offer one or two merit badge classes each month to the Scouts of Troop 370 in areas requested by the Patrol Leaders Council.

<u>Monthly Scout Skills Program Coordinator</u> (Sheri Buehner) – Work with merit badge counselors, parents and Scout Leaders to identify short programs (20-45 minutes) to present to the Scouts as part of the weekly meeting (after approval and scheduling by the Patrol Leaders Council)

Contact Sheri Buehner if you are interested in being a Merit Badge Counselor, helping with the Troop’s Merit Badge Counselor Program or would like to submit a proposal for the Scout Skills Program.

_<u>Used Gear Sale</u>:_

<u>Collection and Compilation</u> – Volunteer to collect and organize “donations” of used camping gear for redistribution

<u>Cashier and Checkout</u>– Volunteer for cashier/check-out duties as the Troop repurposes used camping gear

Cyrus Sharp coordinated the Troop’s first sale in February 2015.  Contact Cyrus if you are interested in assisting with the next sale.

## Troop Banquet (August)

<u>Setup and/or Breakdown</u> - Volunteer to assist event coordinator with set up before or breakdown after the banquet.

<u>Grocery Getter</u> – Assist the event coordinator in purchasing any supplies and/or food for the banquet.

Fontaine Kohler is the principal coordinator for the Troop’s annual awards banquet.  Contact Fontaine if you are interested in helping with the banquet.

## Eagle Courts of Honor

<u>Setup and/or Breakdown</u> - Volunteer to assist event coordinator with set up or breakdown after the reception.

<u>Eagle Photographer</u> – Volunteer to take pictures of the ceremony, Eagle Scouts and families and post the pictures to Smug Mug or other photo sharing site.  This allows the Eagle families to enjoy the day without having to worry about memorializing it. 

<u>Grocery Getter</u> – Assist the event coordinator in purchasing any supplies and/or food for the reception, including picking up the cake.

Stacy Cimowsky coordinates the Courts of Honor.  Contact Stacy if you are interested in assisting with these important ceremonies.

## Summer Camp – Woodruff

<u>Registration</u> – Be one of the several volunteers to arrive early and assist with Summer Camp check-in, and break down stations afterwards.

<u>Gym Supervisors</u> – Watch over the kids congregating in the gym after check in but prior to departure.

<u>Grocery Getter</u> - Volunteer to make a Costco or Sam’s Club Run for cracker barrel provisions

<u>Health and Safety Assistant</u> – Help the Health Form Tracker collect and organize Health Forms prior to Summer Camp.

<u>Medicine Master and Assistants</u> – Be part of the team that insures that all Scouts take regularly prescribed medicines, including checking in the medicine before camp.

<u>Adult Campers</u> – The Troop needs adult volunteers to stay at camp for all or a portion of the week.  There are plenty of Scouts to supervise and mentor.

Jeff Hunt is the Woodruff Summer Camp coordinator.  Contact Jeff if you would like to assist at Summer Camp.

## Winter Camp – Bert Adams

<u>Coordinator</u> – Be the adult coordinator responsible for publicizing this 5-day event that occurs between Christmas and New Years Day and for getting the Scouts registered and signed up for merit badges.

<u>Adult Campers</u> – The Troop needs 2-5 adult volunteers to stay at camp for all or a portion of the 5-day camp.  There are plenty of Scouts to supervise and mentor.

## Troop Equipment

<u>Repair Dude</u> - Work with Quartermaster to identify equipment that needs to be repaired and work with other interested adults and Scouts to complete repairs.

<u>Grocery Getter</u> – Work with Quartermaster to be sure that Troop Staples are in stock for campouts and resupply as necessary.

John Snelson is the Camping Equipment Coordinator and works with the Troop Quartermaster.  Contact John if you are interested in assisting with the Troop equipment and the Quartermaster.

## Pine Straw Weekends (twice a year)

<u>Registration, Food and Beverage Tables</u> – Volunteer to track Scout attendance and hours at the bi-annual pine straw sales.

<u>Marketing</u> – Work with the Pine Straw Coordinator to direct and coordinate marketing prior to the event

<u>Orders Clerk</u> – Work with the Pine Straw Coordinator to direct and coordinate the receipt, logging and tracking of orders prior to the event, including mapping out delivery routes and schedules

<u>Distribution Coordinator</u> - Work with the Pine Straw Coordinator and Orders Clerk to map out delivery routes and schedules.

<u>Delivery Dude</u> - Volunteer your truck or pull a trailer to deliver pine straw

<u>Grocery Getter</u> – Volunteer to make a Costco, Sam’s Club and Pizza Run for snacks and lunch

Franziska Shepard is the Troop’s Pine Straw Coordinator, in charge of this biannual event to help Scouts pay for summer camp, high adventure trips and jamborees.  Please contact Franziska if you are interested in playing one of the parts described above.

## Fund Raising and Service – Once a Year Programs

Camp Cards for Summer Camp and High Adventure – Spring (Franziska Shepherd)

Scout Popcorn Sales - Fall

Scouting for Food – March (Michael Maley)

Tex Woodruff coordinates many of the Troops fundraising efforts.  Please call Tex if you are interested with helping out on any of these programs.

## Grocery Run Gang: Volunteer to Make a Grocery Run for Local Troop Events:  (Duplicative of Above)

- Merit Badge Lunches
- Scout Weekend Snack Bar
- Summer Camp Cracker Barrels
- Pine Straw Lunch and Snacks
- Eagle Courts of Honor
- Annual Troop Banquet

## Assistant Scoutmaster Advancement and Program Roles

Assistant Scoutmasters in Troop 370, among other things, help to monitor and encourage the learning and mastery of Scouting skills which leads to advancement in rank from Tenderfoot up to Eagle Scout.  The Assistant Scoutmasters work closely with the Scout-aged Instructors and Troop Guides to be sure that our young Scouts are given the opportunity to learn and advance.

Our Scoutmaster, Harry Evans, is the leader of the Assistant Scoutmaster Corps. Contact Harry if you would like more information about being an Assistant Scoutmaster with Troop 370.

## Email Information for Selected Adult Scouters

| Position                    | Name               | Email                        |
|-----------------------------|--------------------|------------------------------|
| Chartered Organization Rep. | Kent Watkins       | kent.watkins@gmail.com       |
| Troop Committee Chairman    | John Buehner       | jebuehnersr@comcast.net      |
| Scoutmaster                 | Harry Evans        | hgevans@bellsouth.net        |
| Venture Advisor             | Leslee Evans       | evansclan@bellsouth.net      |
| Advancement Chairman        | Trigger Williams   | triggerw@bellsouth.net       |
| Treasurer                   | Emory Boyd         | emoryboyd@hotmail.com        |
| Membership/Re-charter       | Walter Waddey      | wwwaddey@bellsouth.net       |
| Campout Coordinator         | Cyrus Sharp        | chsharp3@gmail.com           |
| Equipment Coordinator       | John Snelson       | rjsnelsonjr@gmail.com        |
| Health and Safety           | Sandy Pon          | sandypon@hotmail.com         |
| Pine Straw Fundraiser       | Franziska Shepard  | franziska@bellsouth.net      |
| Summer Camp Chairman        | Jeff Hunt          | iconherder@gmail.com         |
| Communication               | Stephanie Sherling | steph.sherling@gmail.com     |
| Troop Fundraising           | Tex Woodruff       | rrwoodruff@gmail.com         |
| Merit Badge Clinics         | Shelli Keagle      | pack370den25@gmail.com       |
| Scout Skills Program        | Sheri Buehner      | sheribuehner@gmail.com       |
| Board of Review Coordinator | Steve Shindell     | steveshindell@gmail.com      |
| Uganda Bridges Program      | Fontaine Kohler    | fontainekohler@bellsouth.net |
| New Parent Liaison          | Stephanie Sherling | steph.sherling@gmail.com     |
