---
layout: md
title: Troop Communications
columns: 2
---

## Reminders

Sign up to receive weekly reminders about troop meetings, Eagle Court of Honor reminders, and meeting plans for troop meetings.

To sign up, you will be required to either provide a phone number, provide an email address, or download the Remind app to your phone or computer.

[Sign up for Troop 370 reminders](https://www.remind.com/join/troop370b){:.mdc-button.mdc-button--outlined.md}

<iframe src="https://widgets.remind.com/widget?height=365&amp;join=true&amp;token=a1841e2083940134cee50242ac110004" id="remind101-widget-0" class="remind101-messages" allowtransparency="true" style="border:0;" title="Remind messages" width="100%" height="365px" frameborder="0"></iframe>

## Email Newsletters

Sign up to receive weekly email newsletters, Eagle Court of Honor annoucements, merit badge announcements, and other additional notices and opprotunities.

[Subscribe to email newsletters](https://visitor.r20.constantcontact.com/manage/optin?v=001C1y_CazDx7-D91frPeh3u2I8M7ZQzW9Q){:.mdc-button.mdc-button--outlined.md}

## Facebook

Join our Facebook group for access to photos from troop events, annoucements related to upcoming opprotuinities, easy communication with Troop 370 members and parents, and other tips and information.

[View Troop Facebook page](https://www.facebook.com/groups/126169727423188/){:.mdc-button.mdc-button--outlined.md}

## SmugMug

View photos from past Troop 370 events.

[View SmugMug page](https://troop370.smugmug.com/){:.mdc-button.mdc-button--outlined.md}
