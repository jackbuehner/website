---
layout: default
title: Online Advancement
redirect_from: /members/online-advancement/index.html
---
<div class="content-strip content-strip--primary content-strip--extra-padding content-strip--center">
  <div class="flex-item">
    <h1>Online Advancement</h1>
    <p>Online courses for merit badges and rank advancement</p>
  </div>
</div>
<div class="content-strip">
  <div class="flex-item">
    <div class="oah-grid-container">
      {% include oah-card.html
        title="Genealogy MB"
        start-date="02 April 2020"
        start-time="7:00 PM"
        counselor="Bill Peck"
        icon="/src/badge-icons/genealogy.png"
        icon-alt-text="genealogy merit badge icon"
        card-href="/members/online-advancement/genealogy.html"
      %}
      {% include oah-card.html
        title="Sustainability MB"
        start-date="01 April 2020"
        start-time="7:30 PM"
        counselor="Sarah Evans"
        icon="/src/badge-icons/sustainability.png"
        icon-alt-text="sustainability merit badge icon"
        card-href="/members/online-advancement/sustainability.html"
      %}
      {% include oah-card.html
        title="Family Life MB"
        start-date="26 March 2020"
        start-time="7:00 PM"
        counselor="Scott Boze"
        icon="/src/badge-icons/familylife.png"
        icon-alt-text="family life merit badge icon"
        card-href="/members/online-advancement/family-life.html"
      %}
      {% include oah-card.html
        title="Railroading MB"
        start-date="26 March 2020"
        start-time="7:00 PM"
        counselor="Bill Peck"
        icon="/src/badge-icons/railroading.png"
        icon-alt-text="railroading merit badge icon"
        card-href="/members/online-advancement/railroading.html"
      %}
      {% include oah-card.html
        title="First Class 9a"
        start-date="25 March 2020"
        start-time="7:30 PM"
        instructor="John Buehner"
        icon="/src/badge-icons/firstclass.png"
        icon-alt-text="first class patch"
        card-href="/members/online-advancement/fc9a.html"
      %}
    </div>
  </div>
</div>
