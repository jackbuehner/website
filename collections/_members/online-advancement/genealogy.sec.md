---
layout: md
title: Genealogy Merit Badge
subtitle: First session on Thursday, April 2nd at 7:00 PM
menu: hide
headings: false
merit_badge: true
folder-id: 1g_-hJzZ1bR4kDw4MJKLA5HrVC6qQpWaH
---

Troop 370 will be offering the Genealogy merit badge through our remote advancement program. Successful completion of this merit badge will include being on an introductory call to be held on Thursday, April 02, 2020 at 7:00 PM via Zoom conference call.

Bill Peck, the course instructor, will send out an email on Thursday morning with the call details. Participants should review the merit badge pamphlet and workbook (located in the class files section below) prior to the first video conference call. We should be able to complete the merit badge over two sessions. Participants will have some work to complete after each call.

To reserve your spot in this class for Thursday, April 02, complete the form below. There is no cost associated with this merit badge. Given the nature of the merit badge and the limitations of remote learning and discussion, this merit badge class will be limited to 12 Scouts. If there is sufficient interest, we will consider additional sessions.

If you have any questions about Genealogy merit badge or this class specifically, please email Bill Peck at [whpeck@me.com](mailto:whpeck@me.com).

[<i class="material-icons mdc-button__icon">how_to_reg</i> Sign up](/members/online-advancement/genealogy-sign-up.html){:.mdc-button.mdc-button--outlined.md}
