---
layout: md
title: First Class Requirement 9a
subtitle: Wednesday, March 25th at 7:30 PM
menu: hide
headings: false
merit_badge: true
folder-id: 1d_bEG1Ns7r6WzfIVOLOTNeALZcMt5wGK
---

Troop 370 will be offering Scouts who have not yet attained First Class rank the opportunity to satisfy First Class Requirement 9(a), which requires the Scout to discuss his Constitutional rights and obligations as a U.S. citizen with a selected individual. John Buehner (who is an attorney – one of the recommended professions in the requirement) will be hosting a small group (up to five) to discuss their rights and obligations as a U.S. citizen. In preparation for this discussion, the Scout should review the applicable pages in the Scout Handbook in the “Citizenship” section and should also read through the Bill of Rights and the other Amendments to the U.S. Constitution. There will not be a test, but it will be helpful for Scouts to be familiar with the topics covered by the constitutional amendments.

The meeting will likely take about a half an hour. If more than five people sign-up, we will have additional sessions.

To reserve your spot in this class to be held on Wednesday, March 25 at 7:30 pm, complete the sign-up form below.

If you have any questions about First Class Requirement 9a or this class, please email John Buehner ([jebuehnersr@comcast.net](mailto:jebuehnersr@comcast.net)) or Harry Evans at [hgevans@bellsouth.net](mailto:hgevans@bellsouth.net)

[<i class="material-icons mdc-button__icon">how_to_reg</i> Sign up](/members/online-advancement/fc9a-sign-up.html){:.mdc-button.mdc-button--outlined.md}