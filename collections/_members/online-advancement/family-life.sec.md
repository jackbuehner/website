---
layout: md
title: Family Life Merit Badge
subtitle: First session on Thursday, March 26th at 7:00 PM
menu: hide
headings: false
merit_badge: true
folder-id: 1OwIPw3STseUiuPsJ2rFysEf7yXqYSHN3
---

Troop 370 be offering the Family Life merit badge through our remote advancement program. Successful completion of this merit badge will involve several phone/video conferences (~60 mins each) with the group and engagement with the Scout's parents & family between sessions. The Scout will need to complete a home project, keep a log of household chores for 90 days and have several "family meetings."

The content of the family meetings include:

1.  Avoiding substance abuse, including tobacco, alcohol, and drugs, all of which negatively affect your health and well-being
1.  Understanding the growing-up process and how the body changes, and making responsible decisions dealing with sex
1.  How your chores in requirement 3 contributed to your role in the family
1.  Personal and family finances
1.  A crisis situation within your family
1.  The effect of technology on your family
1.  Good etiquette and manners

To reserve your spot in this class for March 26th at 7 pm (or to express interest in another session), complete the form below. There is no cost associated with this merit badge.

If you have any questions about Family Life merit badge or this class specifically please email Scott Boze ([scotthboze@gmail.com](mailto:scotthboze@gmail.com)) or Harry Evans at [hgevans@bellsouth.net](mailto:hgevans@bellsouth.net).

[<i class="material-icons mdc-button__icon">how_to_reg</i> Sign up](/members/online-advancement/family-life-sign-up.html){:.mdc-button.mdc-button--outlined.md}
