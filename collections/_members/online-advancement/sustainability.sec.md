---
layout: md
title: Sustainability Merit Badge
subtitle: First session on Wednesday, April 1st at 7:30 PM
menu: hide
headings: false
merit_badge: true
folder-id: 1e0qvfURLUNc3Krmbd7Vy0dYB7acUZUrO
---

Troop 370 will be offering the Sustainability merit badge through our remote advancement program. This is an "optionally" required merit badge for Eagle (you must have either Environmental Science or Sustainability). It is not an easy merit badge and will require several 30 day projects around the home. Successful completion of this merit badge will also involve several phone/video conferences (~60 mins each) with the group and engagement with the Scout's parents & family between sessions. The Scout will also need two "family meetings." It is, however, a very good merit badge and a very timely time to work on it.

To reserve your spot in this class for this Wednesday, April 1st at 7:30 pm (or to express interest in another session), complete the form below. There is no cost associated with this merit badge.

If you have any questions about Sustainability merit badge or this class specifically please email the counselor Sarah Evans at [sarahcagnion@gmail.com](mailto:sarahcagnion@gmail.com) or Harry Evans at [hgevans@bellsouth.net](mailto:hgevans@bellsouth.net).

[<i class="material-icons mdc-button__icon">how_to_reg</i> Sign up](/members/online-advancement/sustainability-sign-up.html){:.mdc-button.mdc-button--outlined.md}
