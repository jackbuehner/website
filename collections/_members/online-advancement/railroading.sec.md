---
layout: md
title: Railroading Merit Badge
subtitle: First session on Thursday, March 26th at 7:00 PM
menu: hide
headings: false
merit_badge: true
folder-id: 187dU39ud4CnWBG0kHR2DtoMAEkvd3yI6
---

Troop 370 will be offering the Railroading merit badge through our remote advancement program. Successful completion of this merit badge will involve being on an introductory call to be held on Thursday, March 26th at 7:00 PM. The merit badge worksheet will be sent to participants. On the introductory call, we will discuss the requirements, our plan, and begin discussion on the railroad industry.

To reserve your spot in this class for March 26th at 7 pm (or to express interest in another session), complete the sign up form below. There is no cost associated with this merit badge.

If you have any questions about Railroading merit badge or this class specifically please email Bill Peck ([whpeck@me.com](mailto:whpeck@me.com)).

[<i class="material-icons mdc-button__icon">how_to_reg</i> Sign up](/members/online-advancement/railroading-sign-up.html){:.mdc-button.mdc-button--outlined.md}
