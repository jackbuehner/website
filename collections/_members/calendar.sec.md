---
layout: md
title: Calendar
subtitle: Upcoming Troop campouts and other events for the 2018-2019 year can be found on the calendar PDF Calendar and the Google Calendar. To import the Troop calendar into your calendar application, download the iCalendar file.</p><p><a href="https://www.dl.dropboxusercontent.com/s/mab2u7uoiylgfz1/Calendar%202019%20-%202020.pdf?dl=0" class="mdc-button mdc-button--outlined">Open PDF calendar</a><a href="https://calendar.google.com/calendar/embed?src=370scouts%40gmail.com&amp;ctz=America%2FNew_York" class="mdc-button mdc-button--outlined">View Google Calendar</a><a href="https://calendar.google.com/calendar/ical/370scouts%40gmail.com/public/basic.ics" class="mdc-button mdc-button--outlined">Download iCalendar file</a>
columns: 2
---
<style>
  #preview {
    display: none;
  }
</style>

## Reminders

Sign up to receive weekly reminders about troop meetings, Eagle Court of Honor reminders, and meeting plans for troop meetings.

To sign up, you will be required to either provide a phone number, provide an email address, or download the Remind app to your phone or computer.

[Sign up for Troop 370 reminders](https://www.remind.com/join/troop370b){:.mdc-button.mdc-button--outlined.md}

## Preview

<iframe src="https://widgets.remind.com/widget?height=365&amp;join=true&amp;token=a1841e2083940134cee50242ac110004" id="remind101-widget-0" class="remind101-messages" allowtransparency="true" style="border:0;" title="Remind messages" width="100%" height="365px" frameborder="0"></iframe>
