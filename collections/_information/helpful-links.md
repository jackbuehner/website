---
layout: md
title: Helpful Links
subtitle: Use the resources below to learn more about the the Boy Scouts of America and the Atlanta Area Council and access useful training documents and videos. Access additional information and forms on our <a href="/information/forms-documents.html">Forms &amp; Documents page</a>.
nav_order: 7
columns: 2
redirect_from: helpful-links.html
---
## Boy Scouts of America

Visit the central website for the Boy Scouts of America to learn about all of the BSA scouting programs and access useful information and training.

[Visit scouting.org](https://www.scouting.org/){:.mdc-button.md}

## Atlanta Area Council

Visit our council's website to access camp, training, volunteer, and contact information for the council.

[Visit atlantabsa.org](https://www.atlantabsa.org/){:.mdc-button.md}

## Uniform Insignia Placement

Learn the guidelines for placement of Uniform Insignia.

[Read guidelines](https://filestore.scouting.org/filestore/pdf/33066/33066_Scouts_BSA_Insignia_WEB.pdf){:.mdc-button.md}

## Guide to Safe Scouting

The Guide to Safe Scouting is an overview of Scouting policies and procedures gleaned from a variety of sources.

[Read guide](https://www.scouting.org/health-and-safety/gss/toc/){:.mdc-button.md}

## REI Guide: How to Pack and Hoist a Backpack (video)

This article offers packing tips and explains the proper way to hoist your pack when it’s full.

[Read guide](https://www.rei.com/learn/expert-advice/loading-backpack.html){:.mdc-button.md}
