---
layout: md
title: FAQs
long_title: Frequently Asked Questions
nav_order: 5
redirect_from: faqs.html
---
## Where can I get help?

If you are having trouble with anything, whether it be a requirement or another scout, go talk to one of the Scouts on the leadership team (the scouts sitting in the front of the room) or any adult. Troop 370 is very lucky to have one of the greatest adult teams in the Scouting World; help will always be given at Troop 370 to those who ask for it.

## Who can sign off on requirements?

The only people who may sign off on requirements are Instructors, the Scoutmaster, Assistant Scoutmasters, Junior Assistant Scoutmasters, and Troop Committee Members.

## What do I need to do to advance to the next rank?

1. Complete all requirements except Scoutmaster Conference and Board of Review and have them signed off by a qualified Scout or Adult.
1. Show up to the next meeting in a complete field uniform (Shirt, Belt, Pants, Socks, and Neckerchief).
1. Talk to Mr. Evans as soon as you are dismissed and ask for a Scoutmaster Conference for your rank. Then, when finished, you will go to a Board of Review.

## What is the Board of Review and Scoutmaster Conference?

The Scoutmaster Conference is a meeting between the Scout seeking advancement and the Scoutmaster (Mr. Evans, the short guy). You have nothing to worry about; there is a 100% pass rate as long as you have completed the requirements. He will ask you some questions and sign you off. Then, you go to the Board of Review; this is a panel of three adults, of which you will probably know the majority. They will talk to you about the rank you completed and what you do outside of Scouting. This is more getting to know the adults in your troop than a test, and again you have nothing to worry about.

## What do I need for a campout?

Check out [Harry’s Packing List](/src/documents/Harry Packing List.pdf) for anything you need on a campout. Always bring rain gear.

## I don’t have [insert camping item here]. Where can I get one?

Troop 370 has a well-stocked supply closet with backpacks, tents, cooking stoves, and cooking utensils. If you need an item, talk to a Quartermaster, and he will help you checkout any camping gear you need.

If you think you are going to need something over and over again, it might be wise to buy one yourself. REI, Cabelas, and High Country Outfitters are all good stores for camping equipment, but you can always talk to another Scout or adult about options or where they got theirs.
