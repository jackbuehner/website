---
layout: md
title: Advancement
subtitle: It is recommended that Scouts attempt to complete one or multiple requirements towards advancement at each meeting. Troop 370 has registered Merit Badge Counselors and Scout Instructors to help Scouts advance. View the list below for advancement resources.
nav_order: 2
columns: 2
redirect_from: advancement.html
---
## Merit Badge Counselors

A list of merit badge counselors and their contact information can be found in the password protected members section of our site.

[Merit badge counselors page](/members/mb-counselors.html){:.mdc-button.md}

## Merit Badge Clinics

Sign up for Merit Badge classes at meritbadge.info.

[meritbadge.info](http://meritbadge.info/){:.mdc-button.md}

## TroopMaster Web

TroopMaster Web allows each scout to track his advancement online.

[TroopMaster Web](https://tmweb.troopmaster.com/mysite/troop370atl){:.mdc-button.md}

## Advancement Requirements

Current requirements for rank advancement and other awards can be found on scouting.org.

[Advancement requirements](https://www.scouting.org/programs/scouts-bsa/advancement-and-awards/){:.mdc-button.md}
