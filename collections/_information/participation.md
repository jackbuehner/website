---
layout: md
title: Scout/Parent Participation
subtitle: Scout and parent participation is crucial to the success of Troop 370. Troop 370 has weekly meetings, goes on monthly campouts, attends multiple summer camps, leads dozens of Eagle Scout service projects, and enjoys multiple high adventure opportunities throughout the year. We encourage every youth scout and adult scouter to participate in these wonderful opportunities provided by the Troop.
nav_order: 3
redirect_from: participation.html
---
## Scout Participation (Youth)

Troop 370 is a boy-led troop, meaning that the scouts lead the meetings and plan the activities and campouts - not the adults. All scouts have the opportunity to take leadership positions in the troop, such as being a Senior Patrol Leader, Assistant Senior Patrol Leader, Instructor, Troop Guide, Patrol Leader, Scribe, Quartermaster, or Historian. Scouts can take leadership positions at any time or for any purpose; they do not need to wait until elections or for when they need it for advancement. One of the primary roles of leaders in the troop is planning the meetings in the monthly PLCs (patrol leaders' councils). Any scout wanting to give their input for the weekly meetings can do so by attending the PLCs. Read more about our leadership in our [Guidelines for Positions of Leadership document](/src/documents/Guidelines%20for%20Positions%20of%20Leadership.pdf).

Advancement moves at the pace of the scout, so he can move as quickly or as slowly as he wants or as time permits. Troop instructors and assistant scoutmasters are happy to help any scout with the advancement requirements.

Overall, there are many opportunities for fun, service, and leadership at the Troop, and scouts can participate in as many or as few activities and opportunities as they decide!

## Scouter Participation (Adults)

For parents, there are many ways in which you can assist through volunteer efforts. There is no greater joy than joining your Scout through their Troop 370 experience and working to support the Troop behind the scenes. If you would like to see how you can help, please contact the Troop, attend a meeting, or check out the "What Can I Do?" link in the password protected members section of the site. Meetings are held most Wednesday evenings from 7:30 PM - 9:00 PM in the St. James UMC activities building.

Using the "Information" drop-down menu on this website, you will find forms, helpful information, and other common documents that you will find helpful during your journey through Scouts at Troop 370.

If you have any questions, ask them at a meeting or contact the Troop with our [contact form](/contact.html)
