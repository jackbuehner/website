---
layout: md
title: Cookie Usage Information
menu: hide
redirect_from: cookies.html
---
We use cookies to collect data on which pages are most visited, how the site was accessed, as well as from what country it was accessed, to provide a better user experience. We track this data with Google Analytics. Google may also use this data at their discretion. You can learn more about the Google Analytics privacy policy [on their website](https://support.google.com/analytics/answer/6004245?hl=en).
