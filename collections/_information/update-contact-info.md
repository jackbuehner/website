---
layout: md
title: Update Contact Information
long_title: Update Your TroopMaster Contact Information
subtitle: To adapt for changes in the provider of our email blasts and to enable the Troop to communicate by text messages, we ask that you change or add your email addresses and cell phone numbers in TroopMaster. This will allow you and your parents to receive email blasts and texts through our new system. We also want to be sure we have the most up to date home phone numbers. Here are the instructions to do so.
nav_order: 8
redirect_from:
  - contact-info-directions.html
  - information/contact-info-directions.html
---
## Steps

1. Start by heading to [the Troop’s TroopMaster page](https://tmweb.troopmaster.com/mysite/troop370atl).
1. Log in (if you have never logged in before, the username is in this format: ‘firstname’.’lastname’ , ex: john.smith . Note: The name used is the name of the scout, not the parent). The password, if not previously reset, is the scout’s BSA membership ID (consists of 9 numbers).
1. In the top nav-bar, click ‘Scouts’. A drop-down menu will appear as shown below. Click on ’Scout Management’. It is the only option provided in the dropdown menu.
![Step 3 Screenshot](/src/troopmaster-contact-info-instructions/pic1.png)
1. You will be presented with an entry for your scout profile. Click the blue 'view' button on the right as shown below.
![Step 4 Screenshot](/src/troopmaster-contact-info-instructions/pic2.png)
1. A window will appear. Click on the blue ‘personal info’ button.
![Step 5 Screenshot](/src/troopmaster-contact-info-instructions/pic3.png)
1. Click ‘edit’ in the top right corner of the window as shown below.
![Step 6 Screenshot](/src/troopmaster-contact-info-instructions/pic4.png)
1. Add or update your home phone number as well as the Scout's cell phone number. Also, update the email address to the Scout's current email. After, click the red ‘save’ button in the top right corner of the window as shown below. Then click close to return to the main menu.
![Step 7 Screenshot](/src/troopmaster-contact-info-instructions/step7.png)
1. To edit contact information for the Scout's parents, click the blue button for the parent whose contact information you would like to change (you can do this for the other parent later) as shown below.
![Step 8 Screenshot](/src/troopmaster-contact-info-instructions/step8.png)
1. Click the blue 'edit' button in the top right of the window.
![Step 9 Screenshot](/src/troopmaster-contact-info-instructions/step9.png)
1. Update the contact information (cell phone and email at the least for optimal communication), then click the red 'save' button in the top right and then the blue 'close' button next to the 'save' button as shown below.
![Step 10 Screenshot](/src/troopmaster-contact-info-instructions/step10.png)
1. To update the contact information for the other parent if wanted, restart from Step 8.
1. Click close. Now, you’re all set!

If you have any questions, feel free to contact the webmaster for assistance using the [webmaster contact page](/contact-webmaster.html)

[<svg class="mdc-button__icon" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" style="height:24px;"><path d="M5,20H19V18H5M19,9H15V3H9V9H5L12,16L19,9Z"></path></svg> Download instructions](/src/documents/Update your TroopMaster Contact Information.docx){:.mdc-button.mdc-button--outlined.md}
