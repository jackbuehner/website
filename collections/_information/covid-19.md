---
layout: md
title: COVID-19 Updates
long_title: Coronavirus (COVID-19) Updates
menu: hide
---
<style>
.content-strip--center {
  background-color: #ffcc00;
  color: black;
}
h2:not(#current-status) {
  font-size: 28px;
  margin-bottom: -14px;
}
</style>

## Current Status

- Wednesday Meetings for March 18, March 25, and April 1 are cancelled (per APS school closures).
- Pine Straw delivery has been postponed to Saturday, April 25. [More details on the Pine Straw page](/pay/pinestaw.html).
- We still plan on hiking and mountain biking in Cloudland Canyon on April 18-19.
- The Troop will be offering online advancememnt opportunities on our [Online Advancement page](/members/online-advancement.html). If you need the password, [contact the webmaster](/contact-webmaster.html).

Questions? [Contact Us](/contact.html).

## Event Update: Meetings, Pine Straw, April Campout, & Online Advancement Opportunities

24 March 2020 at 1:13 PM

TO: Troop 370 <br> 
FROM: John Buehner, Troop Committee Chair

As noted earlier, the Troop has cancelled Wednesday night meetings for March 25 and for April 1 (consistent with APS school closures). APS's Spring Break includes the April 8 meeting, so the Troop will make a decision on the April 8th meeting next week. The City of Atlanta "stay at home" order expires April 6\. As you might expect, if the Mayor extends the order, the Troop will not meet. This will also confirm that pine straw delivery has been postponed until April 25\. As of now, we are still planning to go hiking and mountain bike riding at Cloudland Canyon on April 18-19.

Check the new Online Advancement Page in the Members Section of the Troop website for opportunities to work on merit badges and rank requirements. Classes and sessions will added from time to time, and the class leader may post relevant materials required or helpful for the class.

A Scout is clean - wash your hands.

John Buehner <br>
Troop Committee Chair

## Troop 370 Events Amid the Coronavirus Outbreak

14 March 2020 at 3:08 PM

TO: Troop 370 <br>
FROM: John Buehner, Troop Committee Chair

As noted earlier, the Spring Camporee (March 20-22) has been cancelled, and the Troop has cancelled Wednesday night meetings for March 18 and March 25 (consistent with APS school closures). For the April 1 meeting, the Troop will follow the schedule set by APS (that is, if APS closes, the Troop will not meet). As of now, the Troop is still planning to deliver pine straw on March 28, and we are still planning to go hiking and mountain bike riding in Cloudland Canyon on April 18-19.

St. James UMC has cancelled its Activities Building events.

Accordingly, Monday's Troop Committee will he held as a conference call (March 16th at 6:30 PM). Connection information has been sent in an email and posted to the [Facebook group](https://www.facebook.com/groups/126169727423188/).

A Scout is clean - wash your hands.

John Buehner <br>
Troop Committee Chair

## Camporee Update

12 March 2020 at 11:30 PM

TO: Troop 370 <br>
FROM: Bill Peck, Phoenix Distrct Chairman

On behalf of the Phoenix District, we made the decision to cancel the Phoenix District Camporee.

This decision was made shortly after Governor Kemp’s comments to Georgia’s educational community urging that they consider closing.

The health and welfare of the scouts in the Phoenix District are a core responsibility for the Council, District, and Unit leaders and volunteers. Accordingly, we made the difficult decision to cancel the District Camporee that was scheduled for March 21 and 22.

Please do what you can do to keep your families safe and healthy.

Bill Peck <br>
Phoenix District Chairman
