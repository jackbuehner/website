---
layout: md
title: Finding MB Class/Counselor
long_title: Finding a Merit Badge Class or Counselor
nav_order: 6
headings: false
redirect_from: finding-mb.html
---

Troop 370 hosts Merit Badge Clinics twice a year. The schedule can be viewed on the Calendar. Scouts can also register for other Merit Badge Clinics. In addition, scouts can reach out to our Merit Badge counselors at any time to work on Merit Badges.

To see all advancement opportunities - including Merit Badge Clinics and a list of Merit Badge counselors - please view the Advancement page.

[Advancement](/advancement.html){:.mdc-button.mdc-button--outlined.md}
